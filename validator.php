<?php
// купа хедерів чисто щоб запити могли надсилатись і прийматись
header('Content-Type: application/json');
header('Access-Control-Allow-Origin: http://127.0.0.1:5501');
header('Access-Control-Allow-Methods: POST, OPTIONS');
header('Access-Control-Allow-Headers: Content-Type');

// options запит знов щоб дозволити отримувати запити```````
if ($_SERVER['REQUEST_METHOD'] === 'OPTIONS') {
    http_response_code(200);
    exit();
}

$data = json_decode(file_get_contents('php://input'), true);


if (empty($data)) {
    header('HTTP/1.1 400 Bad Request');
    echo json_encode(array('error' => 'No data received.'));
} else {
    $response = array('valid' => true, 'errors' => array());

    if (!ctype_alpha($data['firstName'])) {
        $response['valid'] = false;
        $response['errors'][] = 'First name must contain only alphabetic characters.';
    }

    if (!ctype_alpha($data['lastName'])) {
        $response['valid'] = false;
        $response['errors'][] = 'Last name must contain only alphabetic characters.';
    }

    if (!preg_match('/^[A-Za-z0-9\-]+$/', $data['group'])){
        $response['valid'] = false;
        $response['errors'][] = 'Group must contain only alphabetic characters and a dash.';
    }

    // Send response
    header('Content-Type: application/json');
    echo json_encode($response);
}
