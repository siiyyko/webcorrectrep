const fields = {
    id : document.getElementById('HiddenId'),
    group : document.getElementById('Group'),
    firstName : document.getElementById('FirstName'),
    lastName : document.getElementById('LastName'),
    gender : document.getElementById('Gender'),
    birthday : document.getElementById('Birthday'),
};

const modalWindow = document.getElementById('modalWindow');
const modalBackground = document.getElementById('modalBackground');

const addNewStudentButton = document.getElementById('addStudentButton');
const tableBody = document.getElementById('mainTable').getElementsByTagName('tbody')[0];
const errorsDiv = document.getElementById('errorsDiv');

let students = [
    {
        id : 0,
        group : "PZ-22",
        firstName : "Oleksii",
        lastName : "Blyzniuk",
        gender : "Male",
        birthday : "23.08.2005",
    }
]

function sendPost(url, userData){
    return fetch(url, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(userData)
    })
    .then((response) => {
        if (!response.ok) {
            throw new Error("Network response was not okay.");
        }
        return response.json();
    })
    .then((jsonData) => {
        console.log("Server response: ", jsonData);
        if (jsonData.errors && jsonData.errors.length > 0) {
            jsonData.errors.forEach(function(error) {
                console.log(error);
                let paragraph = document.createElement('p');
                paragraph.textContent = error;
                errorsDiv.appendChild(paragraph);
            });
            return false;
        }
        return true;
    })
    .catch((error) => {
        console.error("Error sending data to server: ", error);
        return false;
    });
}

function openModal(isEditMode, closestButton){
    if(isEditMode){
        fillFields(closestButton);
        changeTopFormLabel('Edit student');
    }
    else{
        changeTopFormLabel('Add student');
    }

    modalWindow.style.display = 'block';
    modalBackground.style.display = 'block';
}

function closeModal(){
    modalWindow.style.display = 'none';
    modalBackground.style.display = 'none';

    eraseFields();
    
}

function eraseFields(){
    for (const fieldName in fields) {
        if (fields.hasOwnProperty(fieldName)) {
            fields[fieldName].value = "";
        }
    }
    fields.gender.value = "Female";

    errorsDiv.innerHTML = '';
}

function changeTopFormLabel(newName){
    document.getElementById('TopFormLabel').innerText = newName;
}

function fillFields(closestButton){
    let row = closestButton.closest('tr');
    let names = row.cells[2].textContent.split(' ');
    fields.id.value = row.id;
    fields.group.value = row.cells[1].textContent;
    fields.firstName.value = names[0];
    fields.lastName.value = names[1];
    fields.gender.value = row.cells[3].textContent;

    var dateParts = row.cells[4].textContent.split('.');
    var isoDate = dateParts[2] + '-' + dateParts[1] + '-' + dateParts[0];
    fields.birthday.value = isoDate;

}

function editStudent(data){
    // students[data.id] = data;

    let row = document.getElementById(data.id);

    console.log(row);

    row.cells[1].textContent = data.group;
    row.cells[2].textContent = data.firstName + ' ' + data.lastName;
    row.cells[3].textContent = data.gender;
    row.cells[4].textContent = data.birthday;
}

function OKbuttonClick(){
    let isEditMode = fields.id.value === '' ? false : true;

    let data = {
        id : fields.id.value,
        group : fields.group.value,
        firstName : fields.firstName.value,
        lastName : fields.lastName.value,
        gender : fields.gender.value,
        birthday : formatDate(fields.birthday.value)
    }

    sendPost('http://localhost/validator.php', data)
    .then((isValid) => {
        if(!isValid){
            return false;
        }
        else{

            if(isEditMode){
                //editStudent(data);
                editStudentDataBase(data);
                //displayAllStudentsFromDataBase();
            } 
            //else insertNewStudent(data);
            else{
                addNewStudentToDataBase(data);
                //displayAllStudentsFromDataBase();
            }
        
            setTimeout(displayAllStudentsFromDataBase(), 5000);

            closeModal();
        }
      })
      .catch((error) => {
        console.error("Error:", error);
        return false;
    });

}

function addNewStudentToDataBase(data){
    fetch('addStudent.php', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(data)
    })
    .then(response => {
        if (!response.ok) {
            throw new Error('Network response was not ok');
        }
        return response.text();
    })
    .then(data => {
        console.log(data); // Log response from server
    })
    .catch(error => {
        console.error('There was a problem with the fetch operation:', error);
    });
}

function editStudentDataBase(data){
    fetch('editStudent.php', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(data)
    })
    .then(response => {
        if (!response.ok) {
            throw new Error('Network response was not ok');
        }
        return response.text();
    })
    .then(data => {
        console.log(data); // Log response from server
    })
    .catch(error => {
        console.error('There was a problem with the fetch operation:', error);
    });
}

function deleteStudentFromDataBase(button){
    let row = button.closest('tr').id;

    let data = {
        id: row
    };

    fetch('deleteStudent.php', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(data)
    })
    .then(response => {
        if (!response.ok) {
            throw new Error('Network response was not ok');
        }
        return response.text();
    })
    .then(data => {
        console.log(data); // Log response from server
    })
    .catch(error => {
        console.error('There was a problem with the fetch operation:', error);
    });
    
    
}

function displayAllStudentsFromDataBase(){
    tableBody.innerHTML = '';
    fetch('selectall.php')
  .then(response => response.json())
  .then(data => {
    for(let stud of data){
        insertNewStudent(stud);
    }

  })
  .catch(error => console.error('Error:', error));
}

displayAllStudentsFromDataBase();

function insertNewStudent(data){
    let newRow = tableBody.insertRow(tableBody.rows.length);

    newRow.id = data.id;

    let checkboxCell = newRow.insertCell(0);
    let groupCell = newRow.insertCell(1);
    let nameCell = newRow.insertCell(2);
    let genderCell = newRow.insertCell(3);
    let birthdayCell = newRow.insertCell(4);
    let onlineCell = newRow.insertCell(5);
    let optionsCell = newRow.insertCell(6);

    let isOnline = Math.floor(Math.random() * 2);
    let onlineTag = isOnline ? '🌹' : '🥀';

    checkboxCell.innerHTML = '<input type="checkbox">';
    groupCell.innerHTML = data.group;
    nameCell.innerHTML = data.firstName + ' ' + data.lastName;
    genderCell.innerHTML = data.gender;
    birthdayCell.innerHTML = data.birthday;
    onlineCell.innerHTML = onlineTag;
    
    var userOptionsDiv = document.createElement('div');
    userOptionsDiv.className = 'userOptions';
    userOptionsDiv.innerHTML = '<i class="fa-solid fa-pen" onclick="openModal(true, this)"></i><i class="fa-solid fa-xmark" onclick="deleteStudentFromDataBase(this)"></i>';
    optionsCell.appendChild(userOptionsDiv);

    let newStudent = {
        id : data.id,
        group : data.group,
        firstName : data.firstName,
        lastName : data.lastName,
        gender : data.gender,
        birthday : data.birthday,
    }
    students.push(newStudent);
}

 function deleteRow(button){
    let row = button.closest('tr').id;

    if(row){
        row.remove();
    }
 }

function padZero(number) {
    return number < 10 ? '0' + number : number;
}

function formatDate(date){
    let selectedDate = new Date(date);
    
    console.log(selectedDate);

    let day = selectedDate.getDate();
    let month = selectedDate.getMonth() + 1;
    let year = selectedDate.getFullYear();

    let formattedDate = padZero(day) + '.' + padZero(month) + '.' + year;

    return formattedDate;
}

addRedCircleClass();

function addRedCircleClass() {
    var bellIcon = document.getElementById('bell');

    setInterval(() => {
      bellIcon.classList.add('display-red-circle');

      bellIcon.addEventListener('mouseenter', function () {
        bellIcon.classList.remove('display-red-circle');
      });
    }, 10000);
}

let isNotifNeedToClose = false;

function showNotifications(){
    var notifBox = document.getElementById('notificationsBox');
    
    if(isNotifNeedToClose){
        notifBox.classList.remove('displayNotifBox');
    }
    else{
        notifBox.classList.add('displayNotifBox');
    }

    isNotifNeedToClose = !isNotifNeedToClose;
}

let isUserBoxNeedToClose = false;

function showUserBox(){
    var userBox = document.getElementById('userBox');

    if(isUserBoxNeedToClose){
        userBox.classList.remove('displayUserBox');
    }
    else{
        userBox.classList.add('displayUserBox')
    }

    isUserBoxNeedToClose = !isUserBoxNeedToClose;
}
