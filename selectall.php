<?php

require_once("config.php");
$query = "SELECT * FROM `students`";
$result = mysqli_query($con, $query) or die(mysqli_error($con));

if ($result->num_rows > 0) {
    $students = array();
    while($row = $result->fetch_assoc()) {
        // Construct a JSON object for each student
        $nameParts = explode(" ", $row['stud_Name']);
        $studentJson = array(
            'id' => $row['id'],
            'group' => $row['stud_Group'],
            'firstName' => $nameParts[0],
            'lastName' => $nameParts[1],
            'gender' => $row['stud_Gender'],
            'birthday' => $row['stud_Birthday']
        );
        // Add the JSON object to the array of students
        //echo json_encode($studentJson);
        $students[] = $studentJson;
    }
    echo json_encode($students);
} else {
    echo "0 results";
}
