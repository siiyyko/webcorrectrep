<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="theme-color" content="#ff928b">
    <title>Document</title>
    <link rel="stylesheet" href="styles.css">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Inconsolata:wght@200..900&display=swap" rel="stylesheet">
    <!-- <link rel="manifest" href="manifest.json"> -->
    <script src="https://kit.fontawesome.com/fb18b8155e.js" crossorigin="anonymous" async></script>
    <script src="normalScript.js" defer></script>
    <!-- <script src="service-worker.js"></script> -->
</head>
<body>
    <header>
        <a href="#">CMS
            <img src="imgs/rose_icon.png" alt="">
        </a>

        <div class="userInfo">
            <i class="fa-regular fa-bell" id="bell" onclick="showNotifications()"></i>
            <div id="notificationsBox">
                <div>
                    <div class="messageInfo">
                        <i class="fa-solid fa-user"></i>
                        <p>Joe B.</p>
                    </div>
                    <div class="message"></div>
                </div>
                <div>
                    <div class="messageInfo">
                        <i class="fa-solid fa-user"></i>
                        <p>Petro P.</p>
                    </div>
                    <div class="message"></div>
                </div>
                <div>
                    <div class="messageInfo">
                        <i class="fa-solid fa-user"></i>
                        <p>Volodymyr Zh.</p>
                    </div>
                    <div class="message"></div>
                </div>
            </div>
            <div class="roundIcon" onclick="showUserBox()">
                <i class="fa-solid fa-user"></i>
            </div>
            <p onclick="showUserBox()">Oleksii Blyzniuk</p>
            <div id="userBox">
                <a href="#">Log Out</a>
                <a href="#">Profile</a>
            </div>
        </div>
    </header>
    <main>
        <nav>
            <ul class="sectionsList">
                <li><a href="#">Dashboard</a></li>
                <li><a href="#" class="sectionChosen">Students</a></li>
                <li><a href="#">Tasks</a></li>
                <li><img src="imgs/another_rose.png" alt="" class="rose_icon"></li>
            </ul>
        </nav>

        <div class="mainWrapper">
            <h2>Students</h2>
            <div class="addStudentButton" id="addStudentButton" onclick="openModal()">
                <i class="fa-solid fa-plus"></i>
            </div>
            <br/>
            <div id="modalBackground"></div>
            <div id="modalWindow">
                <div class="topBlock">
                    <p id="TopFormLabel">Add student</p>
                    <i class="fa-solid fa-xmark modalCross" onclick="closeModal()"></i>
                </div>
                <form>
                    <div id="errorsDiv"></div>
                    <div>
                        <label for="Group">Group</label>
                        <input name="Group" type="text" id="Group" maxlength="8" required>
                    </div>
                    <div>
                        <label for="FirstName">FirstName</label>
                        <input name="FirstName" type="text" id="FirstName" maxlength="16" required>
                    </div>
                    <div>
                        <label for="LastName">LastName</label>
                        <input name="LastName" type="text" id="LastName" maxlength="16" required>
                    </div>
                    <div>
                        <label for="Gender">Gender</label>
                        <select name="Gender" id="Gender" required>
                            <option value="Female">Female</option>
                            <option value="Male">Male</option>
                        </select>
                    </div>
                    <div>
                        <label for="Birthday">Birthday</label>
                        <input name="Birthday" type="date" id="Birthday" required>
                    </div>
                    <input type="hidden" id="HiddenId">
                </form>
                <div class="bottomBlock">
                    <button onclick="OKbuttonClick()">Ok</button>
                    <button onclick="closeModal()">Cancel</button>
                </div>
            </div>
            <div class="tableWrapper">
                <table id="mainTable">
                    <thead>
                        <th><input type="checkbox"></th>
                        <th>Group</th>
                        <th>Name</th>
                        <th>Gender</th>
                        <th>Birthday</th>
                        <th>Status</th>
                        <th>Options</th>
                    </thead>
                    <tr id="0">
                        <td><input type="checkbox"></td>
                        <td>PZ-22</td>
                        <td>Oleksii Blyzniuk</td>
                        <td>Male</td>
                        <td>23.08.2005</td>
                        <td>🌹</td>
                        <td>
                            <div class="userOptions">
                                <i class="fa-solid fa-pen" onclick="openModal(true, this)"></i>
                                <i class="fa-solid fa-xmark" onclick="deleteRow(this)"></i>
                            </div>
                        </td>
                    </tr>
                    
                </table>
            </div>
        </div>
        
    </main>

</body>

<!-- <script>
    if ('serviceWorker' in navigator) {
      window.addEventListener('load', function() {
        navigator.serviceWorker.register('service-worker.js').then(function(registration) {
          console.log('ServiceWorker registration successful with scope: ', registration.scope);
        }, function(err) {
          console.log('ServiceWorker registration failed: ', err);
        });
      });
    }
  </script> -->

</html>