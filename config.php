<?php

$dbhost = "localhost";
$dbuser = "root";
$dbpass = "";
$dbname = "students_db";

if(!$con = mysqli_connect($dbhost, $dbuser, $dbpass, $dbname))
{
    die("Failed to connect with error: ". mysqli_connect_error());
}